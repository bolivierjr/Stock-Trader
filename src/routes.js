import Home from './components/Home';
import Portfolio from './components/portfolio/Portfolio';
import Stocks from './components/stocks/Stocks';

export const routes = [
  { path: '/', component: Home, name: 'home' },
  { path: '/portfolio', component: Portfolio, name: 'portfolio' },
  { path: '/stocks', component: Stocks, name: 'stocks' }
];
